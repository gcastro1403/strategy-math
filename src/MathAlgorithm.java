
public interface MathAlgorithm {

	void calculate(int num1, int num2);
	
}
