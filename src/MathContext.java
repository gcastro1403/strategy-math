
public class MathContext {
	
	private MathAlgorithm mathAlgorithm;
	
	public MathContext(MathAlgorithm mathAlgorithm) {
		
		this.mathAlgorithm = mathAlgorithm;
		
	}
	
	public void execute (int num1, int num2) {
		
		mathAlgorithm.calculate(num1, num2);
	}
}
