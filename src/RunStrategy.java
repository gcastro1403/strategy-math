
public class RunStrategy {

	public static void main(String[] args) {
		
		MarthSubstract sub = new MarthSubstract();
		MathMultiply mult = new MathMultiply();
		
		MathContext contextSub = new MathContext(sub);
		MathContext contextMult = new MathContext(mult);
		
		contextSub.execute(2, 8);
		contextMult.execute(3, 9);
		
		//Output console
		
		//10
		//27

	}

}
